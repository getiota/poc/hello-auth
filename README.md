# Hello World

A simple dockerized hello world in go with echo.

## Guidelines

This project respects the [google guidelines](https://github.com/golang-standards/project-layout).

## Utils

### Generate EC 256 key

```
openssl ecparam -genkey -name prime256v1 -noout -out key.pem
openssl ec -in key.pem -pubout -out key-pub.pem
```