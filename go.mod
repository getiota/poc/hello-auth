module getiota.eu/api/auth

go 1.15

require (
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.4.0
)
