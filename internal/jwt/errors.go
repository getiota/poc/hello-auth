package jwt

import "errors"

// Error Constants
var (
	ErrInvalidClient = errors.New("Invalid Client")
	ErrKeyDoesNotExist = errors.New("Key does not exist")
)
