package jwt

import (
	"encoding/base64"
)

// An equivalent to EncodeToString that returns bytes
func b64Encode(raw []byte) []byte {
	encoded := make([]byte, base64.RawURLEncoding.EncodedLen(len(raw)))
	base64.RawURLEncoding.Encode(encoded, raw)
	return encoded
}
