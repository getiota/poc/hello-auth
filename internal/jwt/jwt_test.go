package jwt

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncodeClaims(t *testing.T) {
	claims := NewClaims("clt", "", []string{"user", "client"}, 100)

	bytes, err := encodeClaims(claims)

	assert.Nil(t, err)
	assert.NotNil(t, bytes)

	// TODO: unmarshal data to check values
}

func TestMake(t *testing.T) {
	claims := NewClaims("client1", "me", []string{"user", "client"}, 100)
	key, _ := _retrieveKey(_fixturePath("client1.pem"))

	token, err := Make(claims, key)
	assert.Nil(t, err)
	t.Log(token)
}

func _fixturePath(name string) string {
	filePath, _ := os.Getwd()
	return path.Join(filePath, "../../fixtures/"+name)
}

func _retrieveKey(keyPath string) (*ecdsa.PrivateKey, error) {
	keyBytes, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return nil, ErrKeyDoesNotExist
	}

	block, _ := pem.Decode(keyBytes)
	key, err := x509.ParseECPrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	return key, nil
}
