package jwt

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestB64Encode(t *testing.T) {
	assert.Equal(
		t,
		[]byte(`SSBhbSB5b3VyIGZhdGhlcg`),
		b64Encode([]byte(`I am your father`)),
	)
}
