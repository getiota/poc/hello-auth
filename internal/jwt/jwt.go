package jwt

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"errors"
)

var header []byte = b64Encode([]byte(`{"alg": "ES256","typ": "JWT"}`))

func encodeClaims(claims *Claims) ([]byte, error) {
	raw, err := json.Marshal(claims)
	if err != nil {
		return nil, err
	}
	return b64Encode(raw), nil
}

func sign(key *ecdsa.PrivateKey, data []byte) ([]byte, error) {
	hash := sha256.Sum256(data)
	var err error

	if key == nil {
		return nil, errors.New("Private key is nil")
	}

	// Adapted from https://github.com/dgrijalva/jwt-go/blob/master/ecdsa.gov
	if r, s, err := ecdsa.Sign(rand.Reader, key, hash[:]); err == nil {
		keyBytes := 256 / 8

		// We serialize the outpus (r and s) into big-endian byte arrays and pad
		// them with zeros on the left to make sure the sizes work out. Both arrays
		// must be keyBytes long, and the output must be 2*keyBytes long.
		rBytes := r.Bytes()
		rBytesPadded := make([]byte, keyBytes)
		copy(rBytesPadded[keyBytes-len(rBytes):], rBytes)

		sBytes := s.Bytes()
		sBytesPadded := make([]byte, keyBytes)
		copy(sBytesPadded[keyBytes-len(sBytes):], sBytes)

		out := append(rBytesPadded, sBytesPadded...)

		return out, nil
	}

	return nil, err
}

// Make builds and signs a JWT
func Make(claims *Claims, key *ecdsa.PrivateKey) (string, error) {
	encodedClaims, err := encodeClaims(claims)
	if err != nil {
		return "", err
	}

	signatureData := append(header, byte('.'))
	signatureData = append(signatureData, encodedClaims...)

	signature, err := sign(key, signatureData)
	if err != nil {
		return "", err
	}

	return string(header) + "." + string(encodedClaims) + "." + string(b64Encode(signature)), nil
}
