package jwt

import (
	"time"
)

// Claims reprensent the main part of the JWT
type Claims struct {
	Issuer    string   `json:"iss,omitempty"`
	Audience  string   `json:"aud,omitempty"`
	Subject   string   `json:"sub,omitempty"`
	Roles     []string `json:"roles,omitempty"`
	IssuedAt  int64    `json:"iat,omitempty"`
	ExpiresAt int64    `json:"exp,omitempty"`
}

// NewClaims is the default constructor for Claims
func NewClaims(audience string, subject string, roles []string, lifetime int64) *Claims {
	iat := time.Now().Unix()
	c := new(Claims)
	c.Issuer = "getiota.fr"
	c.Audience = audience
	c.Subject = subject
	c.Roles = roles
	c.IssuedAt = iat
	c.ExpiresAt = iat + lifetime

	return c
}
