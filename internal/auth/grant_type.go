package auth

import (
	"errors"
	"net/url"
)

// GrantType is an enum for OAuth2 grant types
type GrantType string

// All allowed grant types
const (
	ClientCredentials GrantType = "client_credentials"
	Password          GrantType = "password"
	RefreshToken      GrantType = "refresh_token"
	AuthorizationCode GrantType = "authorization_code"
)

func validatePasswordGrant(params url.Values) (username string, password string, err error) {
	username = params.Get("username")
	password = params.Get("password")

	if len(username) == 0 || len(password) == 0 {
		err = errors.New("Missing credentials")
	}
	return
}

func validateRefreshGrant(params url.Values) (refreshToken string, err error) {
	refreshToken = params.Get("refresh_token")
	if len(refreshToken) == 0 {
		err = errors.New("Missing credentials")
	}
	return
}
