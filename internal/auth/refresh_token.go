package auth

var _refreshTokens = make(map[string]string)

// Generates and stores a new refresh token for the user
func newRefreshToken(userID string) (refreshToken string, err error) {
	refreshToken, err = generateSecret(32)
	if err != nil {
		return
	}

	for key, element := range _refreshTokens {
		if element == userID {
			delete(_refreshTokens, key)
		}
	}

	_refreshTokens[refreshToken] = userID
	return
}

func userFromRefreshToken(refreshToken string) (userID string, found bool) {
	userID, found = _refreshTokens[refreshToken]
	return
}
