package auth

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// ErrorCode is a possible code returned by the token route
type ErrorCode string

// The possible error codes
const (
	InvalidRequest       ErrorCode = "invalid_request"
	InvalidClient        ErrorCode = "invalid_client"
	InvalidGrant         ErrorCode = "invalid_grant"
	UnauthorizedClient   ErrorCode = "unauthorized_client"
	UnsupportedGrantType ErrorCode = "unsupported_grant_type"
	InvalidScope         ErrorCode = "invalid_scope"
)

// ErrorResponse is what is returned in case of an error
type ErrorResponse struct {
	Code ErrorCode `json:"error"`
}

func fail(c echo.Context, code ErrorCode) error {
	return c.JSON(http.StatusBadRequest, ErrorResponse{code})
}
