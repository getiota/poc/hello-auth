package auth

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRefreshToken(t *testing.T) {
	userID := "thega"

	token1, _ := newRefreshToken(userID)

	assert.NotEmpty(t, token1)

	u, found := userFromRefreshToken(token1)
	assert.Equal(t, userID, u)
	assert.True(t, found)

	token2, _ := newRefreshToken(userID)

	assert.NotEqual(t, token1, token2)

	u, found = userFromRefreshToken(token1)
	assert.Empty(t, u)
	assert.False(t, found)

	u, found = userFromRefreshToken(token2)
	assert.Equal(t, userID, u)
	assert.True(t, found)
}
