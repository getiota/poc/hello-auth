package auth

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuildPrivatKey(t *testing.T) {
	client := Client{}
	client.PrivateKeyString = "-----BEGIN EC PRIVATE KEY-----\nMHcCAQEEIDtIX/49aHFCW+lRt+Z3bZ0Cm6Qn/s3gYmVt80WuaaY+oAoGCCqGSM49\nAwEHoUQDQgAEcHvFnq9EqjRIPv++mij8C4ZuAWzggxyVd+yhAczRHSWU3Z9mx1pb\nyDlqmyihgwvtrgN7CFw1sME3cT/T1dc9Ew==\n-----END EC PRIVATE KEY-----"

	_ = client.buildPrivateKey()

	assert.NotNil(t, client.privateKey)
}
