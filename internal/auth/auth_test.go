package auth

import (
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindClient(t *testing.T) {
	filePath, _ := os.Getwd()
	err := InitClients(path.Join(filePath, "../../fixtures/clients.json"))

	assert.Nil(t, err)

	t.Run("FindClientSuccess", func(t *testing.T) {
		client := FindClient("lu5tYjSel521BgnmXzytxv6WXpTqkVHR",
			"3AAVbdQeQItrJ_USp7YllF7OK2LkrRJArzNowb-HXfxJozP6uxMbSuZmSULArgUt")
		assert.NotNil(t, client)
		assert.Equal(t, "client1", client.Slug)
	})

	t.Run("FindClientWrongPWD", func(t *testing.T) {
		client := FindClient("lu5tYjSel521BgnmXzytxv6WXpTqkVHR",
			"3AAVbdQeQItrJ_USp7YllF7OK2LkrRJArzNowb-HXfxJozP6uxMbSuZmSULArgU")
		assert.Nil(t, client)
	})

	t.Run("FindClientWrongID", func(t *testing.T) {
		client := FindClient("lu5tYjSel521BgnmXzytxv6WXpTqk",
			"3AAVbdQeQItrJ_USp7YllF7OK2LkrRJArzNowb-HXfxJozP6uxMbSuZmSULArgUt")
		assert.Nil(t, client)
	})

	// Test that we cannot modify the original client
	t.Run("FindClientModify", func(t *testing.T) {
		client := FindClient("lu5tYjSel521BgnmXzytxv6WXpTqkVHR",
			"3AAVbdQeQItrJ_USp7YllF7OK2LkrRJArzNowb-HXfxJozP6uxMbSuZmSULArgUt")
		assert.NotNil(t, client)

		client.Slug = "Hello"

		client = FindClient("lu5tYjSel521BgnmXzytxv6WXpTqkVHR",
			"3AAVbdQeQItrJ_USp7YllF7OK2LkrRJArzNowb-HXfxJozP6uxMbSuZmSULArgUt")
		assert.Equal(t, "client1", client.Slug)
	})
}
