package auth

import (
	"fmt"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBasicAuth(t *testing.T) {
	generator := func(auth string, username string, password string) func(t *testing.T) {
		return func(t *testing.T) {
			u, p, _ := parseBasicAuth(auth)
			assert.Equal(t, username, u)
			assert.Equal(t, password, p)
		}
	}

	t.Run("Correct", generator("Basic aGVsbG86bWU=", "hello", "me"))
	t.Run("No Prefix", generator("aGVsbG86bWU=", "", ""))
	t.Run("Not b64", generator("aGVsbG86bW?", "", ""))
	t.Run("Empty", generator("", "", ""))
}

func TestGenerateSecret(t *testing.T) {
	generator := func(length int) func(*testing.T) {
		return func(t *testing.T) {
			generated, err := generateSecret(length)
			t.Log(generated)
			assert.Equal(t, length, len(generated))
			assert.Nil(t, err)
		}
	}

	t.Run("32", generator(32))
	t.Run("33", generator(33))
	t.Run("34", generator(34))
}

func readClients(t *testing.T, fileName string) []Client {
	filePath, _ := os.Getwd()
	filePath = path.Join(filePath, fmt.Sprintf("../../fixtures/%s.json", fileName))

	clients, err := clientsFromJSON(filePath)
	if err != nil {
		t.Fatalf("Could not read clients with:\n%s", err)
	}
	return clients
}

func TestValidClients(t *testing.T) {
	clients := readClients(t, "clients")
	err := validateClients(clients)

	assert.Nil(t, err)

	for _, c := range clients {
		assert.NotNil(t, c.privateKey)
	}
}
