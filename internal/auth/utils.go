package auth

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

var notSlugRegex = regexp.MustCompile(`[^a-z0-9-]`)

func validSlug(s string) bool {
	match := notSlugRegex.Find([]byte(s))
	return match == nil
}

func parseBasicAuth(auth string) (username string, password string, err error) {
	if !strings.HasPrefix(auth, "Basic ") {
		err = errors.New("Invalid prefix")
		return
	}

	b, err := base64.StdEncoding.DecodeString(auth[6:])
	if err != nil {
		return
	}

	parts := strings.SplitN(string(b), ":", 3)
	if len(parts) != 2 {
		err = errors.New("Invalid credentials pattern")
	}

	username = parts[0]
	password = parts[1]
	return
}

func generateSecret(length int) (string, error) {
	enc := base64.RawStdEncoding
	key := make([]byte, 1+enc.DecodedLen(length))
	if _, err := rand.Read(key); err != nil {
		return "", err
	}
	return enc.EncodeToString(key)[:length], nil
}

// ------------------------------------------------
// Building Clients lists

func clientsFromJSON(path string) (clients []Client, err error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bytes, &clients)
	if err != nil {
		return
	}

	return
}

func validateClients(clients []Client) error {
	ids := make(map[string]bool)
	slugs := make(map[string]bool)

	for i := range clients {
		c := clients[i]

		if ids[c.ID] {
			return fmt.Errorf("Duplicate client id %s", c.ID)
		}
		ids[c.ID] = true

		if slugs[c.Slug] {
			return fmt.Errorf("Duplicate client slug %s", c.Slug)
		}
		slugs[c.Slug] = true

		if err := clients[i].validate(); err != nil {
			return err
		}
	}
	return nil
}
