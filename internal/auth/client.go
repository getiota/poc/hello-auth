package auth

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"net/url"

	"getiota.eu/api/auth/internal/jwt"
)

const defaultLifetime int64 = 60 * 60

// Client holds the required data to identify clients
type Client struct {
	ID     string `json:"id"`
	Secret string `json:"secret"`

	Slug string `json:"slug"`

	PrivateKeyString string `json:"private_key"`

	// A map grant_type / roles
	Roles map[GrantType][]string `json:"roles"`

	privateKey *ecdsa.PrivateKey
}

// ------------------------------------------------
// Methods

func (c *Client) buildPrivateKey() error {
	block, _ := pem.Decode([]byte(c.PrivateKeyString))
	key, err := x509.ParseECPrivateKey(block.Bytes)
	if err != nil {
		return err
	}
	c.privateKey = key
	return nil
}

func (c *Client) validate() error {
	if len(c.Slug) == 0 || !validSlug(c.Slug) {
		return fmt.Errorf("Invalid Client Slug %s", c.ID)
	}

	if len(c.ID) != 32 {
		return fmt.Errorf("Invalid Client Slug %s: should 32 chars", c.ID)
	}

	if len(c.Secret) != 64 {
		return fmt.Errorf("Invalid Client Slug %s: should 64 chars", c.ID)
	}

	if err := c.buildPrivateKey(); err != nil {
		return fmt.Errorf("Invalid client Key for slug %s with:\n%s", c.Slug, err)
	}

	return nil
}

func (c *Client) verify(secret string) bool {
	return c.Secret == secret
}

func (c *Client) supports(grantType GrantType) bool {
	_, found := c.Roles[grantType]
	return found
}

func (c *Client) authenticate(params url.Values) (*TokenResponse, ErrorCode) {
	grantType := GrantType(params.Get("grant_type"))
	if !c.supports(grantType) {
		return nil, InvalidRequest
	}

	var claims *jwt.Claims
	var refreshToken = ""

	switch grantType {
	case ClientCredentials:
		claims = c.authenticateClient()
	case Password:
		if u, p, err := validatePasswordGrant(params); err == nil {
			claims, refreshToken = c.authenticateUser(u, p)
			if claims == nil {
				return nil, InvalidGrant
			}
		} else {
			return nil, InvalidGrant
		}
	case RefreshToken:
		if t, err := validateRefreshGrant(params); err == nil {
			claims, refreshToken = c.authenticateRefresh(t)
			if claims == nil {
				return nil, InvalidGrant
			}
		} else {
			return nil, InvalidGrant
		}
	default:
		return nil, UnsupportedGrantType
	}

	if claims == nil {
		return nil, InvalidRequest
	}

	accessToken, err := jwt.Make(claims, c.privateKey)
	if err != nil {
		// TODO: add logs! and change error...
		return nil, InvalidScope
	}

	response := new(TokenResponse)
	response.AccessToken = accessToken
	response.ExpiresIn = defaultLifetime
	response.TokenType = "Bearer"
	response.RefreshToken = refreshToken
	return response, ""
}

func (c *Client) authenticateClient() *jwt.Claims {
	return jwt.NewClaims(c.Slug, "", c.Roles[ClientCredentials], defaultLifetime)
}

func (c *Client) authenticateUser(username string, password string) (*jwt.Claims, string) {
	if username != "thega" || password != "ageht" {
		return nil, ""
	}

	userID := "f5874d54-3058-49ab-b59a-de8022d4141f"

	return c.userClaims(userID)
}

func (c *Client) authenticateRefresh(refreshToken string) (*jwt.Claims, string) {
	userID, found := userFromRefreshToken(refreshToken)
	if !found {
		return nil, ""
	}

	return c.userClaims(userID)
}

func (c *Client) userClaims(userID string) (claims *jwt.Claims, refreshToken string) {
	claims = jwt.NewClaims(c.Slug,
		userID,
		c.Roles[Password],
		defaultLifetime)

	if c.supports(RefreshToken) {
		refreshToken, _ = newRefreshToken(userID)
	}
	return
}
