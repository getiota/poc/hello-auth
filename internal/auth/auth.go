package auth

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

var clients map[string]Client

// InitClients builds the client list from JSONs
func InitClients(path string) error {
	list, err := clientsFromJSON(path)
	if err != nil {
		return err
	}

	if err = validateClients(list); err != nil {
		return err
	}

	clients = make(map[string]Client)
	for _, c := range list {
		clients[c.ID] = c
	}
	return nil
}

// FindClient retrieves a client based on credentials
// Copy the item to ensure that the client is not modified
func FindClient(id string, secret string) *Client {
	// The value is copied here
	client, found := clients[id]
	if !found || !client.verify(secret) {
		return nil
	}
	return &client
}

// BasicAuthMiddleware finds the client and updates the context
func BasicAuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		username, password, err := parseBasicAuth(c.Request().Header.Get(echo.HeaderAuthorization))
		if err != nil {
			return echo.ErrBadRequest
		}
		client := FindClient(username, password)
		if client == nil {
			return echo.ErrUnauthorized
		}
		c.Set("client", client)
		return next(c)
	}
}

// Token returns an access token when possible
// TODO: separate functions to make tests easier
func Token(c echo.Context) error {
	client, ok := c.Get("client").(*Client)
	if !ok {
		c.Logger().Error("Could not find client")
		return fail(c, InvalidRequest)
	}

	values, err := c.FormParams()
	if err != nil {
		return fail(c, InvalidRequest)
	}

	response, errCore := client.authenticate(values)
	if response == nil {
		return fail(c, errCore)
	}

	return c.JSON(
		http.StatusOK,
		response,
	)
}
