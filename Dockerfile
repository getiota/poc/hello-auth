FROM golang:alpine as builder

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

RUN apk --no-cache add make

ARG PORT=8000
ARG BINNAME=auth
ARG CLIENTS_FILE_PATH=/secrets/clients.json

EXPOSE $PORT

WORKDIR /build

# Install dependencies
COPY go.sum go.mod ./
RUN go mod download
RUN go mod verify

# Copy sources
COPY cmd ./cmd
COPY internal ./internal
COPY Makefile ./

# Build binary
RUN GOOS=linux GOARCH=amd64 EXT_LDFLAGS='-w -s' make build

# Preparing files to be copied into scratch
RUN mv /build/bin/$BINNAME /build/bin/main

FROM scratch

COPY --from=builder /build/bin/main /app/

USER 1000

WORKDIR /app

CMD ["./main"]
