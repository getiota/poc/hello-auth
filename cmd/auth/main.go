package main

import (
	"log"
	"net/http"

	"getiota.eu/api/auth/internal/auth"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var exposedPort string
var clientsFilePath string

func main() {

	if err := auth.InitClients(clientsFilePath); err != nil {
		log.Fatalf("Could not load clients for path '%s' with:\n%s", clientsFilePath, err)
	}

	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.POST("/auth/token", auth.Token, auth.BasicAuthMiddleware)
	e.GET("/auth", hello)
	e.GET("/", helloRoot)

	// Start server
	e.Logger.Fatal(e.Start(":" + exposedPort))
}

func helloRoot(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, Root!")
}

func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}
